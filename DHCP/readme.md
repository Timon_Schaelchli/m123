[TOC]

  
# Auftrag 1 «DHCP-Dienst anwenden und verstehen»

 Ich habe mir zuerste denn Auftrag durchgelesen und dann überlget was ich ändern muss. 
 Als erstes habe ich beim Client 1 und 2 ein Häcklein bei "Use DHCP for configuration"(Bild 1). Somit wird der DHCP Server die Ip-Adresse von denn beiden 
 Clienten vergeben sowie verwalten.
 Als nächstes habe ich die Mac-Adresse und Ip-Adresse von Client 3 beim DHCP Server statisch hinzugefügt(Bild 2). 
 Als letztes habe ich denn DHCP Server aktiviert(Bild 3), dann klappte alles.

<br>
Foto 1
![Foto1](./Images/01.png)
<br>
Foto 2
![Foto2](./Images/02.png)
<br>
Foto 3
![Foto3](./Images/03.png)


# Auftrag 2  «DHCP mit Cisco Packet Tracer»


**Auftrag 1:**

**Für welches Subnetz ist der DHCP Server aktiv?**

Der DHCP-Server ist für das Subnetz 192.168.30.1 bis 192.168.30.254 aktiv.(Pool 1)

**Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)**

|IP address   |    Client-ID/      |    Lease expiration     |  Type   |    
|-------------|------------------|----------------------|------------|                 
|192.168.30.31  |  00D0.BC52.B29B     |      --            |   Automatic|
|192.168.30.33   | 0050.0F4E.1D82       |    --             |  Automatic|
|192.168.30.32    |00E0.8F4E.65AA     |      --            |   Automatic|
|192.168.30.34  |  0007.ECB6.4534       |    --            |   Automatic|
|192.168.30.35   | 0009.7CA6.4CE3      |     --            |   Automatic|
|192.168.30.36   | 0001.632C.3508       |    --             |  Automatic|

**In welchem Range vergibt der DHCP-Server IPv4 Adressen?**

192.168.30.1 - 192.168.30.254

**Was hat die Konfiguration ip dhcp excluded-address zur Folge?**

Der Befehl, hat zur Folge, dass bestimmte IP-Adressen im angegebenen Bereich von der dynamischen IP-Adressvergabe durch den DHCP-Server ausgeschlossen werden.

**Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?**

Er kann insgesamt 254 Ip-Adressen vergeben. 


**Auftrag 2:**

**Welcher OP-Code hat der DHCP-Offer?**

OP:0x0000000000000002 

**Welcher OP-Code hat der DHCP-Request?**

OP:0x0000000000000001 

**Welcher OP-Code hat der DHCP-Acknowledge?**

OP:0x0000000000000002 

**An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?**

Ein DHCP-Discover-Paket wird als Broadcast-Paket gesendet. Die Ziel-IP-Adresse in einem DHCP-Paket ist die Broadcast-Adresse im lokalen Netzwerk. (Bei mir zbs. 255.255.255.255)


**An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?**

Das Discover-Paket wird an die Broadcast-MAC-Adresse FF:FF:FF:FF:FF:FF gesendet. Speziell darin ist das es sich bei dieser Mac-Adresse um die Broadcast-MAC-Adresse handelt. 

**Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?**

Weil das Ziel des Discovers an alle geht. Der Switch leitet sie also an alle weiter auch um denn DHCP-Server im Netzwerk zu finden und eine IP-Adresse für denn Laptop vom DHCP-Server zu erhalten.

**Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?**

Nein es gitb kein DHCP-Relay in unserem Netzwerk. Der DHCP-Relay-Agent kommt typischerweise ins Spiel, wenn sich der DHCP-Server in einem anderen Subnetz oder VLAN als die DHCP-Clients befindet.

**Welche IPv4-Adresse wird dem Client zugewiesen?**

192.168.30.33

![Foto4](./Images/04.png)

# Auftrag 3 «DHCP-Sever aufsetzten»

**Einführung**
    <p>Als erstes habe ich denn Auftrag durchgelesen und bei der Vorlage bei denn anderen geschaut. So hatte ich schon ein Bild vor mir wie ich vorgehen muss.

 **Vorbereitung**
    <p>Bevor ich mit dem eigentlichen Konfiguriren des DHCP's Server begonnen habe, musst ich ein paar einstellungen einstellen. Als erstes habe ich natürlich die VM(Ubuntu), auf der VM Workstadion heruntergeladen. Danach habe ich unter "Use ISO image File" ***(Bild 1)*** denn Ubuntu Server ausgewählt. Als nächstes musst ich die Netzwerkadapter richtig einstellen. Das habe ich so gemacht beim M117_Lab ***(Bild 2)***, und beim VMnet ***(Bild 3)***. 

***Bild 1:***
![Iso_Image](./Images/DHCP-Server/iso_Image.png)
***Bild 2:***
![M117_Lab](./Images/DHCP-Server/117_Lab.png)
***Bild 3:***
![VMnet](./Images/DHCP-Server/net.png)



 **Ubuntu Server konfiguration**
    <p>Nachdem ich mit der Vorbereitung fertig war, habe ich die VM gestartet und mit der Ubuntu Server Konfiguration begonnen. Die Mehrheit konnte ich einfach durchskippen ohne etwas ändern zu müssen. Es gab aber auch Ausnahmen. Die Ausnahmen sieht man uf denn 2 Bildern. Auf dem ***Bild*** unten sieht man wie alles heruntergeladen wurde.

***Bild:***
![foto-heruntergeladen-ubu](./Images/DHCP-Server/heruntergeladen.png)



**DHCP Server konfiguration**
     <p>Nach dem Konfigurieren und herunterladen des Ubuntu Servers, habe ich denn DHCP-Server konfiguriert. Als erstes habe ich über das Terminal denn DHCP-Server heruntergeladen. Das habe ich mit dem Befehl ***"sudo apt install isc-dhcp-server"*** gemacht. Das ganze wurde dann heruntergeladen.(Foto)
    <br>

***"sudo apt install isc-dhcp-server":***
![foto-herunterladen](./Images/DHCP-Server/dhcp%20herunterladen.png)


Nach dem herunterladen habe ich mit dem Befehl ***"sudo nano /etc/dhcp/dhcpd.conf"*** die Datei geöffnet. In dieser Datei habe ich denn DHCP-Server konfiguriert. Nach dem man den Befehl eingegeben hat, muss man weit nach unten gehen, bis man die Konfiguration sieht. Im Bild unten sieht man wie ich es konfiguriert habe.(Foto)

 ***"sudo nano /etc/dhcp/dhcpd.conf:"*** 
 ![foto-dhcp-konfi](./Images/DHCP-Server/dhcp%20konfi%20fertig.png)

Nach dem das Konfigurieren geklappt hatte, macht ich denn nächsten Schritt. Mit dem Befehl ***"sudo nano /etc/default/isc-dhcp-server"***, bei INTERFACEv4 eth’0 auf ens34 gewechselt. Das ganze habe ich gespeichert und bin zum nächsten Schritt gegangen(Foto).

***"sudo nano /etc/default/isc-dhcp-server":***
![Foto-Konfi](./Images/DHCP-Server/interface%20.png)

Als nächstes habe ich mit dem Befehl ***"sudo nano /etc/netplan/00-installer-config.yaml"*** dem Netzwerkadapter Ens34 eine Ip-Adresse gegeben. Das ganze sieht man auf dem Bild(Foto). 

 ***"sudo nano /etc/netplan/00-installer-config.yaml":***
 ![Foto-netplan](./Images/DHCP-Server/netplan.png)

**DHCP Server überprüfung**

Zum alles überprüfen ob es funktioniert, habe ich denn Befehl ***ip a*** verwendet. Wie man im Foto sieht stimmt alles.

***ip a:***
![Foto-ipa](./Images/DHCP-Server/beweis.png)

Als letzten Schritten habe ich denn DHCP-Servers restartet. Das mit dem Befehl: ***sudo systemctl restart isc-dhcp-server***. Mit ***sudo systemctl status isc-dhcp-server*** habe ich denn Status des DHCP-Servers überprüfen. Im Bild sieht man das Endresultat.  Alles läuft wie geplant.

***sudo systemctl status isc-dhcp-server:***
![Foto-status](./Images/DHCP-Server/beweis%20activ.png)

Mit ***sudo cat /var/lib/dhcp/dhcpd.leases*** kann man auch die Leases sehen welche der DCP-Server vergibt.

***sudo cat /var/lib/dhcp/dhcpd.leases:***
![Leases](./Images/DHCP-Server/leases.png)

**Client-Konfiguration**

Natürlich wollte ich das ganze noch an einem Client ausprobieren. Dafür habe ich mich mit der Sota-VM angemeldet. Dafür ging ich in denn Einstellungen unter Adapteroptionen, dann Ethernet 1, dann auf IPV4 und dort die Auswahl auf IP-Adresse automatisch beziehen anklicken. Jetzt haben ich alles konfiguriert.

![Foto-IPV4](./Images/DHCP-Server/client-ip.png)
![Foto-Ip-Beziehen](./Images/DHCP-Server/client-ip-dhcp-zuweisung.png)

Als letzter Schritt öffnete ich das Command Prompt und gab ipconfig /release ein, um die aktuelle IP-Adresse freizugeben und gab ipconfig /renew ein, um eine neue zu holen. Wie man im Bild sieht klappte es und ich bekam eine gültige IP-Adresse vom DHCP-Server.

***ipconfig /release:***
![Ip-Release](./Images/DHCP-Server/ipconfig-release.png)

***ipconfig /renew:***

![Ip-Renew](./Images/DHCP-Server/ipconfig-renew.png)


**Troubleshooting**

Anfangs hatte ich einige Probleme. Meine VM-Workstadion frierte mehrmals ein, da es viel Speicher brauchte. Ich hatte beim Konfigurieren des DHCP-Servers auch immer wieder kleine Fehler. Beispielsweise habe ich nicht immer auf die gross- und kleinschreibung geachtet. Bei dem Befehl wo man bei INTERFACEv4 von eth’0 auf ens34 weschseln musste, habe ich als erstes das E gross geschrieben. Daher klappte es nicht. nachdem ich es aber korrigiert habe indem ich da e bei "ens34" kleingeschrieben habe, klappte es wieder. Schlussendlich funktionierte alles.



    


