[TOC]

  
# Auftrag 1 «DNS-Server aufsetzen»

**Einführung**
    <p> Bevor ich anfing, habe ich mir denn Aufrag durchgelesen und mir überlegt, wir ich am besten vorgehen sollte. Als ich das wusste fing ich mit dem Auftrag an. 


**Vorbereitung**
Als erstes habe ich den Client und auch den Server aufgesetzt. Beim Client musste ich kaum was machen, ausser denn Netzwerk Adapter 117Lab hinzuzufügen (Bild unterhalb).

![117Lab Adapter](./Images/client%20adapter.jpg)

 Beim Server musste ich schon mehr machen. Als erstes habe ich die Vm aus dem Internet heruntergeladen danach habe ich eine neue Vm erstellt. In der neuen Vm unter Einstellungen, habe ich bei ISO Image file denn zuvor heruntergeladenen Server hinzugefügt. (Bild untenhalb). 

![ISO Image](./Images/IOS%20Image.png)

Als nächstes habe ich auf beim Server die Adapter einstellungen gemacht. Diese sieht man im Bild (Bild unterhalb).Das war alles was ich an Vorbereitung machen musste.

![Server-Adapter](./Images/Server%20adapter.png)

 **DNS-Server herunterladen**

 Als nächstes habe ich die Server VM gestartet und die Konfiguraation begonnen. Als die Vm lief, habe ich denn Server-Manager gestartet. Dort habe ich dann auf "Rollen und Features hinzufügen" geklickt.(Bild unterhalb)

![DNS-Server1](./Images/DNS%20herunterladen%201.png)

Als nächstes habe ich im neuen Fenster welches auftauchen sollte, die "Rollenbasierte oder featurebasierte Installation" ausgewählt.

![DNS-Server2](./Images/DNS%20herunterladen%202.png)

Danach bei der Serverauswahl habe ich meinen Server ausgewählt. Normalerweise ist dort auch nur 1 Option, welche man auswählen kann (Bild unterhalb).

![DNS-Server3](./Images/DNS%20herunterladen%203.png)

Dann unter Serverrollen habe ich logischerweise denn DNS-Server ausgewählt (Bild unterhalb).

![DNS-Server4](./Images/DNS%20herunterladen%204.png)

Die nächsten Schritten kann man einfach immer weiterklicken, dann sollte der DNS-Server wie geplant heruntergeladen werden.


 **DNS-Server konfigurieren**

 Nachdem der Server heruntergeladen wurde muss man ihn logischweise noch konfigurieren. 

 Dies macht man indem man wieder beim Server-manager unter Tools auf denn DNS-Server klickt (Foto unterhalb).

 ![DNS-Konfi1](./Images/DNS-Konfi1.png)

 Danach kommt man im neuen Fenster in den DNS-Manager. Als erstes muss man dort eine neue Zone erstellen. Dies macht man indem man rechtklick auf das Symbol klickt. Das ganze sieht man im Bild unterhalb.

 ![DNS-Konfi2](./Images/DNS-Konfi2.png)

 Danach kann man wie in denn Bilder unten die Einstellungen machen.
 Zuerst macht man eine Forward-Lookupzone, danach eine Reverse-Lookupzone. 

Forward-Lookupzone:

![DNS-Konfi3](./Images/Neue%20Zone%201%20auswahl.png)

![DNS-Konfi4](./Images/Neue%20Zone%202%20auswahl.png)

![DNS-Konfi5](./Images/Neue%20Zone%203%20auswahl.png)

Die nächsten Schritte kann man dann einfach bestätigen.

Reverse-Lookupzone:

Nun muss man wie bereits erwähnt noch eine Reverse-Lookupzone machen. Als erstes macht man wieder einen rechtsklick und geht wie bei der Forward-Lookupzone auf "Neue Zone".
Die nächsten Schritten sind auch hier bildlich dargestelt


![DNS-Konfi6](./Images/Neue%20Zone%20reverse%201.png)

![DNS-Konfi7](./Images/Neue%20Zone%20reverse%202.png)

![DNS-Konfi8](./Images/Neue%20Zone%20reverse%203.png)

Nachdem man die Schritte befolgte kann man die nächsten Schritte einfach bestätigen. 

So jetzt hat man eine Forward- und eine Reward-Lookupzone erstellt. 
Man ist aber noch nicht ganz fertig. 
Als nächstes muss man in der Forward-Lookupzone indem man auf die bereits erstellt Forward-Lookupzone rechtsklick klickt, Neue Hosts (A oder AAAA) erstellen. Das sieht man auch im Bild unterhalb
![DNS-Konfi9](./Images/Neue%20Zone%20reverse%204.png)

Als nächstes kommt ein neues Fenster dort muss man einmal einen Neuen Host für denn Client und für denn Server erstellen mit denn jeweiligen IP-Adressen. 

![DNS-Konfi10](./Images/Neue%20Zone%20reverse%205.png)

![DNS-Konfi11](./Images/Neue%20Zone%20reverse%206.png)

Das selbe macht man nun in der erstellten Reverse-Lookupzone. Dort klickt man auch rechtsklick drauf und dann geht man auf "Neuer Zeiger PTR" Denn ganzen Ablauf sieht man unten wieder bildlich dargestellt.

![DNS-Konfi12](./Images/Neue%20Zone%20reverse%207.png)

![DNS-Konfi13](./Images/Neue%20Zone%20reverse%208.png)

![DNS-Konfi14](./Images/Neue%20Zone%20reverse%209.png)

Jetzt muss man noch etwas kleines hinzufügen. Und zwar geht man bei dem Symbol wo man die Zonen erstellte, auf Eigenschaften. (Bild unterhalb)

![DNS-Konfi17](./Images/Neue%20Zone%20reverse%2012.png)

Danach geht man auf "Weiterleitung", dort fügt man dann denn DNS-Server von Google hinzu. (Bild unterhalb)

![DNS-Konfi18](./Images/Neue%20Zone%20reverse%2013.png)

Zum alles überprüfen, kann man die 2 Bilder unterhalb anschauen. Wenn man es so hat sollte es funktiionieren.

Forward-Lookupzone:

![DNS-Konfi15](./Images/Neue%20Zone%20reverse%2010.png)

Reverse-Lookupzone:

![DNS-Konfi16](./Images/Neue%20Zone%20reverse%2011.png)


 **DNS-Server Überprüfung**

 Als letzten Schritt muss man natürlich überprüfen ob alles klappt.

 Dies macht man indem man das CMD auf dem Client öffnet und nslookup eingibt. Wenn der DNS-Server angegeben wird, war der Test erfolgreich. (Bild unterhalb)

 ![DNS-Konfi19](./Images/Neue%20Zone%20reverse%2014.png)

als nächstes habe ich folgendes eingegebe: nslookup "Server-IP" (hier: 192.168.100.50). Damit wird getestet, ob der DNS-Server die IP erfolgreich auflösen kann und uns den dazugehörigen Namen gibt. (Bild unterhalb)

![DNS-Konfi20](./Images/Neue%20Zone%20reverse%2015.png)

Zum Schluss habe ich nslookup google.com eingegeben. Mit diesem Befehl wird getestet, ob der von uns konfigurierter DNS die Anfrage an dns.google weiterleitet, wie im Forwarder festgelegt. (Bild unterhalb)

![DNS-Konfi21](./Images/Neue%20Zone%20reverse%2016.png)

Wie man sieht funktioniert alles:).


