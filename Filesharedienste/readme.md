[TOC]

# Auftrag 1 «Linux File-Share»

**Einführung**
    <p> Bevor ich anfing, habe ich mir denn Aufrag durchgelesen und überlegt, was ich alles machen muss, damit der File-Share funktioniert. Als ich das wusste fing ich mit dem Auftrag an. 


**Umsetzung**
   <p> Das erste was ich machte, ist natürlich, eine neue Vm zu erstellen und habe als ISO-Image die Ubunte-Desktp-Version ausgewählt. Dort habe ich dann Linux heruntergeladen.


Als nächstes als Linux heruntergeladen worden ist, habe ich den Befehl ***"Sudo apt update"*** verwendet

![Update](./Images/1%20befehl.png)

Als nächstes habe ich, mit dem Befehl ***"Sudo apt install Samba"***, Samba heruntergeladen. 

![Update](./Images/2%20befehl.png)

Nachdem es huntergeladen war, habe ich mit dem nächsten befehl und zwar ***"systemctl status smbd --no-pager -l"*** denn Status von Samba überprüft. 

![Update](./Images/befehl%203%20überprüfen.png)


Danach habe ich denn Befehl ***"sudo systemctl enable --now smbd"*** eingegeben. Damit startet Samba automatisch.

![Update](./Images/befehl%204%20auto%20start.png)

Als nächstes habe ich den Befehl ***"sudo ufw allow samba"*** verwendet, dieser Befehl erlaubt das Samba benutzt werden kann ohne Probleme mit er Firewall.

![Update](./Images/befehl%205%20samba%20firewall%20erlauben.png)

Dann habe ich den ersten User erstellt, welcher benötigt wird um auf die geteilte Datei später zugreiffen zu können. dies mit dem Befehl ***"sudo usermod -aG sambashare $USER"***.

![Update](./Images/befehl%206.%20User%20erstellt.png)

Danach habe ich gerade noch ein Passwort für den erstellten User frestgelegt. Befehl lautet: ***"sudo smbpasswd -a $USER"***

![Update](./Images/befehl%207%20passwort%20neuer%20user%20gesetzt.png)

Dann war es soweit und ich habe im Ordner Pictures ein Ordner erstellt. Auf dem Ordner Pictures habe denn Share eingerichtet.

![Update](./Images/8.%20neuer%20ordner%20erstellt,%20share%20eingerichtet.png)

Danach habe ich mich mit dem Windows-Client auf den geteilte Share-Ordner angemeldet.

![Update](./Images/9.%20mit%20share%20folder%20verbinden.png)

Dann musste ich denn Benutzname und das Passwort für den Benutzer eingeben welchen ich bereits erstellt hatte, danach konnte ich mich auf den Ordern verbinden.

![Update](./Images/10.%20Verbinden.png)

So sollte es dann am Ende aussehen. Ich kann in dem Ordner neues erstellen oder löschen. Somit hat der Auftrag geklappt;)

![Update](./Images/11.%20verbindet%20kann%20sachen%20löschen,%20erstellen.png)